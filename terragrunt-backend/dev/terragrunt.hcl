include {
  path = find_in_parent_folders()
}

terraform {
  source = "tfr:///terraform-aws-modules/ec2-instance/aws?version=4.0.0"
}

inputs = {
  instance_type = "t2.medium"
  tags = {
    Name = "Terragrunt Tutorial DEV: EC2"
  }
}