generate "backened" {
  path = "backened.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
  terraform {
   backend "s3" {
    bucket         = "terra-bucket-grunt"
    key            = "${path_relative_to_include()}/instance/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "my-lock-table"
  }
  }
EOF
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
    provider "aws" {
      profile = "default"
      region  = "us-east-1"
      }
EOF
}

inputs = {
  ami           = "ami-06ca3ca175f37dd66"
  tags = {
    Name = "Terragrunt Tutorial: EC2"
  }
}