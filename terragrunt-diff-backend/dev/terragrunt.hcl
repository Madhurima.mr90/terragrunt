include "root" {
  path = find_in_parent_folders()
}

generate "backend" {
  path = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
  terraform {
   backend "s3" {
    bucket         = "env-terraformstate-thru-terragrunt"
    key            = "Dev/terraform.tfstate"
    region         = "us-east-1"
    encrypt        =  true
    dynamodb_table = "terraform-dev-lock-table"
  }
  }
EOF
}

terraform {
  source = "tfr:///terraform-aws-modules/ec2-instance/aws?version=4.0.0"
}

inputs = {
  instance_type = "t2.medium"
  tags = {
    Name = "Terragrunt-Dev Tutorial: EC2"
  }
}